//
//  YoutubeListItemModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 03.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import Foundation

class YoutubeListItem: Identifiable {
    var titles: String = ""
    var vid: String = ""
    var poster: String = ""
    var uuid: String = UUID().uuidString
    
    init(titles: String, vid: String, poster: String) {
        self.titles = titles
        self.vid = vid
        self.poster = poster
    }
}
