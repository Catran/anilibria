//
//  FilterStateModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 01.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

struct FilterState {
    var isComplite: Bool
    var state: Int
    var years: String?
    var genres: String?
    var seasons: String?
}
