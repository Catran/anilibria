//
//  InfoReleaseModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 29.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

struct InfoReleaseModel: Decodable {
    let geners: [String]?
    let voices: [String]?
    let description: String?
    let count: Int
    let season: Int
    let day: Int
}
