//
//  ReleasesListItemModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 01.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import Foundation

class ReleasesListItem: Identifiable {
    var id: Int = -1
    var titles: [String] = [""]
    var description: String = ""
    var poster: String = ""
    var uuid: String = UUID().uuidString
    
    init(id: Int, titles: [String], description: String, poster: String) {
        self.id = id
        self.titles = titles
        self.description = description
        self.poster = poster
    }
}
