//
//  FeedModel.swift
//  Anilibria
//
//  Created by  Piter Iaskevich on 10.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

struct Feed: Decodable {
    let release: Release
    let youtube: Youtube
}
