//
//  YoutubeModel.swift
//  Anilibria
//
//  Created by  Piter Iaskevich on 10.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation
import SwiftUI

struct Youtube: Identifiable, Decodable {
    let id: Int
    let title: String
    let image: String
    let vid: String
    let views: Int
    let comments: Int
    let timestamp: Int
}
