//
//  BasicResponseModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 12.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

struct BasicReleasesResponse: Decodable {
    let status: Bool
    let data: [Release]?
    let error: Error?
}

struct BasicListResponse: Decodable {
    let status: Bool
    let data: ReleasePagination
    let error: Error?
}

struct BasicFeedResponse: Decodable {
    let status: Bool
    let data: [Feed]
    let error: Error?
}

struct BasicSchedluleResponse: Decodable {
    let status: Bool
    let data: [Schedule]
    let error: Error?
}

struct BasicOneReleaseResponse: Decodable {
    let status: Bool
    let data: Release
    let error: Error?
}

struct BasicRandomReleaseResponse: Decodable {
    let status: Bool
    let data: RandomRelease
    let error: Error?
}

struct BasicStringsResponse: Decodable {
    let status: Bool
    let data: [String]
    let error: Error?
}

struct BasicYoutubeResponse: Decodable {
    let status: Bool
    let data: YoutubePagination
    let error: Error?
}

// --MARK: Helper model
// Helper error model
struct Error: Decodable {
    let code: Int
    let massage: String?
    let description: String?
}

// Helper ReleasePagination model
struct ReleasePagination: Decodable {
    let items: [Release]?
    let pagination: Pagination
}

// Helper YoutubePagination model
struct YoutubePagination: Decodable {
    let items: [Youtube]?
    let pagination: Pagination
}

