//
//  ReleaseModel.swift
//  Anilibria
//
//  Created by  Piter Iaskevich on 10.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation
import SwiftUI

struct Release: Identifiable, Decodable {
    let id: Int
    let code: String?
    let names: [String]?
    let series: String?
    let poster: String?
    let favorite: Favorite?
    let last: String?
    let moon: String?
    let status: String?
    let type: String?
    let genres: [String]?
    let voices: [String]?
    let year: String?
    let day: String?
    let description: String?
    let blockedInfo: BlockedInfo
    let playlist: [Series]?
    let season: String
}
