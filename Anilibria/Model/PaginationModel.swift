//
//  PaginationModel.swift
//  Anilibria
//
//  Created by  Piter Iaskevich on 10.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

struct Pagination: Decodable {
    let page: Int
    let perPage: Int
    let allPages: Int
    let allItems: Int
}
