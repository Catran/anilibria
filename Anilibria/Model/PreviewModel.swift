//
//  PreviewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 13.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct PreviewModel: Identifiable {
    var index = UUID()
    let id: Int
    let title: String
    let image: Image
    let description: String?
}
