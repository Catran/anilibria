//
//  SeriesModel.swift
//  Anilibria
//
//  Created by  Piter Iaskevich on 10.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation
import SwiftUI

struct Series: Identifiable, Decodable {
    let id: Int
    let title: String
    let sd: String
    let hd: String
    let fullhd: String?
    let srcSd: String?
    let srcHd: String?
}
