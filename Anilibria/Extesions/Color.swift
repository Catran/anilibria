//
//  Color.swift
//  Anilibria
//
//  Created by Петр Яскевич on 28.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

extension Color {
    static let invertColor = Color("invertColor")
    static let invertClear = Color("invertClear")
    struct anilibria {
        static let red = Color("mainRed")
        static let darkRed = Color("darkRed")
    }
}
