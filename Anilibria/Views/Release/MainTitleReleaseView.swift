//
//  MainTitleRelease.swift
//  Anilibria
//
//  Created by Петр Яскевич on 28.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct MainTitleRelease: View {
    let titles: [String]
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    var body: some View {
        VStack {
            Text(titles[0])
                .font(Font.system(size:26))
                .fontWeight(.bold)
                .foregroundColor(colorScheme == .light ? Color.anilibria.red : Color.white)
                .multilineTextAlignment(.center)
                .padding(.horizontal)
            Text(titles[1])
                .font(Font.system(size:20))
                .foregroundColor(colorScheme == .light ? Color.anilibria.darkRed : Color.white)
                .multilineTextAlignment(.center)
                .padding(.horizontal)
        }
    }
}

struct MainTitleRelease_Previews: PreviewProvider {
    static var previews: some View {
        MainTitleRelease(titles: ["Некоторое название", "Some titl"])
    }
}
