//
//  GalleryEpisode.swift
//  Anilibria
//
//  Created by Петр Яскевич on 25.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct GalleryEpisode: View {
    let id: Int
    let model: [Series]
    private let maxEpisode = 6
    @State private var showAllSireas = false
    
    var body: some View {
        VStack{
        ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .center, spacing: 25) {
                    if !model.isEmpty {
                        ForEach(0..<(self.model.count > maxEpisode ? maxEpisode : self.model.count), id: \.self) { i in
                            PreviewEpisode(id: self.id, series: self.model[i])
                        }
                    }
                }.padding(.horizontal, 30)
            }
            if !model.isEmpty {
                NavigationLink(destination: AllEpisodeView(id: self.id, model: model)) {
                    Text("Показать все серии")
                        .font(.headline)
                        .foregroundColor(Color.anilibria.darkRed)
                        .padding(.top, UIScreen.main.bounds.height / 60)
                }.padding(.horizontal)
            }
        }
    }
}

struct GalleryEpisode_Previews: PreviewProvider {
    static var previews: some View {
        GalleryEpisode(id: 0, model: [
            Series(id: 1, title: "Some title", sd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", hd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", fullhd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcSd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcHd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"),
            Series(id: 2, title: "Some title", sd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", hd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", fullhd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcSd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcHd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"),
            Series(id: 3, title: "Some title", sd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", hd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", fullhd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcSd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcHd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"),
            Series(id: 4, title: "Some title", sd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", hd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", fullhd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcSd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcHd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"),
            Series(id: 5, title: "Some title", sd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", hd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", fullhd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcSd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcHd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"),
            Series(id: 6, title: "Some title", sd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", hd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", fullhd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcSd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcHd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"),
        ])
    }
}
