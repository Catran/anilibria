//
//  InfoRelease.swift
//  Anilibria
//
//  Created by Петр Яскевич on 28.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct InfoRelease: View {
    let release: Release
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @State private var showDescription = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            if self.release.status != "Завершен" {
                HStack {
                    Spacer()
                    Text("День выхода серий")
                        .font(.footnote)
                        .foregroundColor(Color.anilibria.darkRed)
                    Spacer()
                }
                HStack {
                    Spacer()
                    WeekdaysView(day: Int(self.release.day ?? "-1")!)
                    Spacer()
                }.padding(.bottom, 30)
            }
            
            if self.release.genres != nil {
                HStack {
                    Text("Жанры")
                        .font(.footnote)
                        .foregroundColor(Color.anilibria.darkRed)
                    Spacer()
                }

                Text(self.release.genres!.joined(separator:", "))
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                    .lineLimit(3)
            }

            HStack {
                Text("Сезон")
                    .font(.callout)
                    .foregroundColor(Color.anilibria.darkRed)
                Text(self.release.season)
                    .font(.subheadline)
                    .foregroundColor(.secondary)

                Spacer()

                Text("Колличество серий")
                    .font(.callout)
                    .foregroundColor(Color.anilibria.darkRed)
                Text("\(self.release.playlist?.count ?? 0)")
                    .font(.subheadline)
                    .foregroundColor(.secondary)
            }
            
            if self.release.voices != nil {
                HStack {
                    Text("Озвучили")
                        .font(.callout)
                        .foregroundColor(Color.anilibria.darkRed)
                    Spacer()
                }
                Text(self.release.voices!.joined(separator: ", "))
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                    .lineLimit(2)
            }
            
            if self.release.description != nil {
                HStack {
                    Text("Описание")
                        .font(.callout)
                        .foregroundColor(Color.anilibria.darkRed)
                    Spacer()
                }
                Text(self.release.description!.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil))
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                    .lineLimit(4)
                HStack {
                    Spacer()
                    Button(action: {self.showDescription = true}) {
                        Text("еще")
                            .font(.callout)
                            .foregroundColor(Color.anilibria.darkRed)
                    }.sheet(isPresented: $showDescription) {
                        NavigationView {
                            ScrollView(.vertical) {
                                VStack {
                                    Text(self.release.description!
                                        .replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil))
                                    .font(Font.system(size: 16))
                                    .foregroundColor(.secondary)
                                    .padding(.horizontal)
                                    .padding(.top, 20)
                                    .navigationBarTitle(Text("Описание"), displayMode: .inline)
                                    Spacer()
                                }
                            }
                        }
                    }
                }
            }
        }.padding(.horizontal, 20)
    }
}

struct InfoRelease_Previews: PreviewProvider {
    static var previews: some View {
        InfoRelease(release: Release(id: 1208, code: "some code", names: ["Волчица и пряности", "Spice and wolf"], series: "12", poster: "s", favorite: nil, last: nil, moon: nil, status: "В работе", type: nil, genres: ["Детектив"], voices: ["Itashi, Lupin"], year: "2019", day: "6", description: "Некоторое описание проекта", blockedInfo: BlockedInfo(blocked: false, reason: nil), playlist: nil, season: "1"))
    }
}


