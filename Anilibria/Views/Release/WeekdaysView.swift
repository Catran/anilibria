//
//  WeekdaysView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 29.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct WeekdaysView: View {
    
    let day: Int
    private let days = ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"]
    
    var body: some View {
        HStack {
            ForEach(days, id: \.self) { item in
                Text(item)
                    .font(Font.system(size: 12))
                    .fontWeight(.semibold)
                    .foregroundColor(self.day - 1 != self.days.firstIndex(of: item)  ? Color.anilibria.darkRed : .invertColor)
                    .frame(width: 20, height: 20)
                    .padding(5)
                    .overlay(
                        Circle()
                            .stroke(Color.anilibria.darkRed, lineWidth: 2)
                    )
                    .background(
                        Circle()
                            .fill(self.day - 1 == self.days.firstIndex(of: item)  ? Color.anilibria.darkRed : .clear)
                    )
            }
        }
    }
}

struct WeekdaysView_Previews: PreviewProvider {
    static var previews: some View {
        WeekdaysView(day: 2)
    }
}
