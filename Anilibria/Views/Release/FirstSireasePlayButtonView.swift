//
//  FirstSireasePlayButton.swift
//  Anilibria
//
//  Created by Петр Яскевич on 28.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct FirstSireasePlayButton: View {
    let id: Int
    let url: String
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    @EnvironmentObject private var userSettings: UserSettings
    
    var body: some View {
        Button(action: {}) {
            Text("начать просмотр")
                .font(Font.system(size:18))
                .foregroundColor(.white)
                .multilineTextAlignment(.center)
                .padding(.bottom, 6)
                .padding(.horizontal, 15)
                .padding(.top, 4)
                .background(
                    Rectangle()
                        .cornerRadius(10)
                        .foregroundColor(Color.anilibria.red)
                )
                .onTapGesture {
                    self.userSettings.currentWatchId = self.id
                    self.viewControllerHolder?.present(style: .fullScreen) {
                        VideoView(url: self.url)
                    }
            }
        }
    }
}

struct FirstSireasePlayButton_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            FirstSireasePlayButton(id: 0, url: "").colorScheme(.dark)
            FirstSireasePlayButton(id: 0, url: "").colorScheme(.light)
        }
    }
}
