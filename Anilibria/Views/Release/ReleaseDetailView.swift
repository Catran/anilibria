//
//  ReleaseDetail.swift
//  Anilibria_test
//
//  Created by  Piter Iaskevich on 15.01.2020.
//  Copyright © 2020  Piter Iaskevich. All rights reserved.
//

import SwiftUI

struct ReleaseDetail: View {
    let id: Int?
    let code: String?
    let image: Image?
    
    @ObservedObject private var viewModel = ReleaseViewModel()

    init(id: Int? = nil, code: String? = nil, image: Image? = nil) {
        self.id = id
        self.code = code
        self.image = image
    }
    
    func load() {
        if id != nil {
            if image == nil {
                self.viewModel.fetchId(id: id!)
            } else {
                self.viewModel.fetchWithoutImage(id: id!, image: image!)
            }
        } else if code != nil {
            self.viewModel.fetchCode(code: code!)
        }
   }
    
    var body: some View {
        GeometryReader { gg in
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    ZStack(alignment: .bottom) {
                        BackgroundImage(image: self.viewModel.poster)
                        VStack {
                            if self.viewModel.release?.names != nil {
                                MainTitleRelease(titles: (self.viewModel.release?.names)!)
                                    .padding(.bottom, 10)
                            }
                            if self.viewModel.firstSeriesUrl != nil {
                                FirstSireasePlayButton(id: self.viewModel.release!.id, url: self.viewModel.firstSeriesUrl!)
                                    .padding(.bottom, UIScreen.main.bounds.height / 30)
                            }
                            if self.viewModel.release?.id != nil {
                                GalleryEpisode(id: self.viewModel.release!.id, model: (self.viewModel.release?.playlist ?? []).reversed())
                                    .padding(.bottom, 20)
                            }
                        }
                    }.frame(width: UIScreen.main.bounds.width, height: gg.size.height)
                    
                    if self.viewModel.release != nil {
                          InfoRelease(release: self.viewModel.release!)
                                .padding(.vertical, 25)
                    }
                    
                }
            }
        }
        .navigationBarTitle(Text(""), displayMode: .inline)
        .onAppear(perform: { self.load() })
    }
}

struct ReleaseDetail_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NavigationView {
                ReleaseDetail(id: 1202, code: "ryuu-no-haisha", image: Image("stubPoster"))
            }
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("iPhone SE")
            
           NavigationView {
                ReleaseDetail(id: 1202, code: "ryuu-no-haisha", image: Image("stubPoster"))
            }
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("iPhone XS Max")
        }
    }
}
