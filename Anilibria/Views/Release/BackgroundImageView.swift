//
//  BackgroundImage.swift
//  Anilibria
//
//  Created by Петр Яскевич on 28.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct BackgroundImage: View {
    let image : Image?
    
    var body: some View {
        ZStack(alignment: .top) {
            if image != nil {
                image!
                    .renderingMode(.original)
                    .resizable()
                    .scaledToFit()

            } else {
                Image("preloadRelease")
                    .renderingMode(.original)
                    .resizable()
                    .scaledToFit()
            }
            
            VStack(spacing: 0) {
                Rectangle()
                    .fill(
                        LinearGradient(gradient:
                            Gradient(
                                colors: [.invertClear, Color.invertColor.opacity(0.5)]),
                                startPoint: .top, endPoint: .bottom))
                    .frame(height: UIScreen.main.bounds.height / 3)
                Rectangle()
                    .fill(
                    LinearGradient(gradient:
                        Gradient(
                            colors: [Color.invertColor.opacity(0.5), .invertColor]),
                            startPoint: .top, endPoint: .bottom))
                    .frame(height: UIScreen.main.bounds.height / 4)
                Rectangle()
                    .foregroundColor(.invertColor)
            }
            
            if image == nil {
                VStack{
                    Spacer()
                    ActivityIndicator(isAnimating: .constant(true), style: .large)
                    Spacer()
                }
            }
        }
    }
}

struct BackgroundImage_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            BackgroundImage(image: Image("stubPoster")).colorScheme(.dark)
            BackgroundImage(image: Image("stubPoster")).colorScheme(.light)
        }
    }
}
