//
//  PreviewEpisode.swift
//  Anilibria
//
//  Created by Петр Яскевич on 25.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import AVKit

struct PreviewEpisode: View {
    let id: Int
    let series: Series
    
    @ObservedObject private var viewModel = EpisodeViewModel()
    @State private var url : String!
    @State private var showPlayer = false
    private var widith:  CGFloat  {
        return UIScreen.main.bounds.width / 2
    }
    private var height:  CGFloat  {
        return UIScreen.main.bounds.width / 3.5
    }
    
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    @EnvironmentObject private var userSettings: UserSettings
    
    func load() {
        if self.viewModel.thumb == nil {
            self.url = series.sd.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
            self.viewModel.fetchThumb(url: self.url)
        }
    }
    
    var body: some View {
        ZStack(alignment: .center) {
            if viewModel.thumb != nil {
                viewModel.thumb!
                    .renderingMode(.original)
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(20)
                    .cornerRadius(100, corners: [.topLeft])
            } else {
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(width: widith, height: height)
                    .cornerRadius(20)
                    .cornerRadius(100, corners: [.topLeft])
            }
            
            
            Text("\(series.id)")
                .font(.callout)
                .foregroundColor(.red)
                .rotationEffect(.degrees(-45))
                .position(x: 8, y: 8)
            
            Text("00:00")
                .font(.subheadline)
                .foregroundColor(.white)
                .position(x: widith - 35, y: height - 15)
           
            Button(action: {
                self.userSettings.currentWatchId = self.id
                self.viewControllerHolder?.present(style: .fullScreen) {
                        VideoView(url: self.url)
                }
            }) {
                PlayButton()
                    .frame(width: 50, height: 50)
            }
        }.frame(width: self.widith, height: self.height).onAppear(perform: { self.load() })
    }
}

struct PreviewEpisode_Previews: PreviewProvider {
    static var previews: some View {
        PreviewEpisode(id: 0, series: Series(id: 1, title: "Some title", sd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", hd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", fullhd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcSd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", srcHd: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"))
            .frame(width: 200, height: 120)
    }
}
