//
//  AllEpisodeView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 30.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct AllEpisodeView: View {
    let id: Int
    let model: [Series]
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @EnvironmentObject private var userSettings: UserSettings
    @State private var invertOrder = false

    private func getUrl(_ url: String)->String {
        return url.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
    }
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Toggle(isOn: $invertOrder.animation(.linear)) {
                    Text("Обратный порядок серий")
                        .foregroundColor(self.colorScheme == .dark ? Color.white : Color.black)
                }.padding(.vertical, 20)
                Divider()
                ForEach(invertOrder ? model.reversed() : model) { (item) in
                    Button(action: {
                        if self.model.first?.id == item.id {
                            self.userSettings.currentWatchId = self.id
                        } else {
                            self.userSettings.currentWatchId = 0
                        }
                        self.viewControllerHolder?.present(style: .fullScreen) {
                            VideoView(url: self.getUrl(item.sd))
                        }
                    }) {
                        HStack {
                            Text(item.title)
                                .font(Font.system(size: 16))
                                .foregroundColor(self.colorScheme == .dark ? Color.white : Color.black)
                            Spacer()
                        }
                    }
                    Divider()
                }
            }
            .padding(.horizontal)
            .padding(.vertical, 10)
        }.navigationBarTitle(Text("Все серии"), displayMode: .inline)
    }
}

struct AllEpisodeView_Previews: PreviewProvider {
    static var previews: some View {
        AllEpisodeView(id: 0, model: [
            Series(id: 1, title: "Some 1", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 2, title: "Some 2", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 3, title: "Some 3", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 4, title: "Some 4", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 5, title: "Some 5", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 6, title: "Some 6", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 7, title: "Some 7", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 8, title: "Some 8", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 9, title: "Some 9", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 10, title: "Some 10", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 11, title: "Some 11", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 12, title: "Some 12", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 13, title: "Some 13", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 14, title: "Some 14", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 15, title: "Some 15", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
            Series(id: 16, title: "Some 16", sd: "", hd: "", fullhd: "", srcSd: "", srcHd: ""),
        ])
    }
}
