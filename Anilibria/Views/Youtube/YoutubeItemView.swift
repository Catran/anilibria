//
//  YoutubeItemView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 03.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct YoutubeItemView: View {
    let model: YoutubeListItem
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(model.titles)
                .font(.headline)
                .lineLimit(1)
                .padding(.horizontal)
            ZStack {
                URLImage(imageUrl: model.poster)
                    .scaledToFill()
                    .frame(maxHeight: UIScreen.main.bounds.height / 3)
                
                NavigationLink(destination: WebView(url: "https://www.youtube.com/embed/\(self.model.vid)")
                    .navigationBarTitle(Text(""), displayMode: .inline)
                ) {
                    HStack {
                        Spacer()
                        PlayButton().frame(width: 75, height: 75)
                        Spacer()
                    }
                }
            }
        }.scaledToFit()
    }
}

struct YoutubeItemView_Previews: PreviewProvider {
    static var previews: some View {
        YoutubeItemView(model: YoutubeListItem(titles: "Хейт от анистар", vid: "zeQtOtNad7o", poster: "https://img1.ak.crunchyroll.com/i/spire1/5601eec39026ea5dbf88fc35d9d6fb9f1516836642_full.jpg"))
    }
}
