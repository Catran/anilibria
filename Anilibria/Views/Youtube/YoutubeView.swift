//
//  YoutubeView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 03.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct YoutubeView: View {
    @ObservedObject var youtubeList = YoutubeList()
    
    func load() {
        self.youtubeList.loadMoreYoutubeItems()
    }
    
    var body: some View {
        VStack {
            if youtubeList.isEmpty {
                Image("ErrorNotFound")
                    .renderingMode(.original)
                    .resizable()
                    .scaledToFit()
                Spacer()
            } else {
                List(youtubeList) { item in
                    YoutubeItemView(model: item)
                        .onAppear(perform: { self.youtubeList.loadMoreYoutubeItems(item) })
                }.buttonStyle(PlainButtonStyle())
            }
        }
    }
}

struct YoutubeView_Previews: PreviewProvider {
    static var previews: some View {
        YoutubeView()
    }
}
