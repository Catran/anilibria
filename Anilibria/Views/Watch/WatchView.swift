//
//  WatchView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 29.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct WatchView: View {
    
    let title: String
    @ObservedObject private var viewModel = WatchViewModel()
    @EnvironmentObject private var userSettings: UserSettings
    
    func load() {
        if self.viewModel.schedule.isEmpty {
            self.viewModel.fetchSchedule()
        }
        
        if self.viewModel.youtube.id == 0 {
            self.viewModel.fetchYotube(1)
        }
        
        if self.viewModel.bests.isEmpty {
            self.viewModel.fetchBest()
        }
        
        self.viewModel.fetchRandomRelease()
    }
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 20) {
                if self.userSettings.currentWatchId != 0 {
                    CurrentWatchView()
                }
                
                if !self.viewModel.schedule.isEmpty {
                    ScheduleCurrentDayView(title: "Ожидается сегодня", expandTitle: "расписание", model: self.viewModel.schedule)
                            .padding(.top, 10)
                }
                
                NavigationLink(destination: ReleaseDetail(code: self.viewModel.randomRelease)) {
                    Text("cлучайное аниме")
                        .font(Font.system(size:20))
                        .foregroundColor(.white)
                        .multilineTextAlignment(.center)
                        .padding(.top, 4)
                        .padding(.horizontal, 25)
                        .padding(.bottom, 6)
                        .background(
                            Rectangle()
                                .cornerRadius(10)
                                .foregroundColor(Color.anilibria.red)
                        )
                }
                
                YoutubevView(model: self.viewModel.youtube)
                
                GalleryView(title: "Лучшее", expandTitle: "", model: self.viewModel.bests)
                
                Spacer()
            }
        }
        .navigationBarTitle(Text(title), displayMode: .automatic)
        .onAppear(perform: { self.load() })
    }
}

struct WatchView_Previews: PreviewProvider {
    static var previews: some View {
        WatchView(title: "Смотреть")
    }
}
