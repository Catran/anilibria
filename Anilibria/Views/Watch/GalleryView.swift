//
//  GalleryView.swift
//  Anilibria_test
//
//  Created by  Piter Iaskevich on 15.01.2020.
//  Copyright © 2020  Piter Iaskevich. All rights reserved.
//

import SwiftUI

struct GalleryView: View {
    let title: String
    let expandTitle: String
    var model: [ReleasesListItem]
    
    var body: some View {
        VStack(spacing: 0) {
            if !title.isEmpty || !expandTitle.isEmpty {
                HStack {
                    Text(title)
                        .font(.headline)
                    Spacer()
                    Text(expandTitle)
                        .font(.headline)
                        .foregroundColor(Color.anilibria.darkRed)
                }.padding(.horizontal)
            }
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 10) {
                    if !model.isEmpty {
                        ForEach(model) { item in
                            NavigationLink(destination:  ReleaseDetail(id: item.id, image: nil)) {
                                VStack(spacing: 10) {
                                    URLImage(imageUrl: item.poster)
                                        .scaledToFit()
                                        .frame(height: 160)
                                        .cornerRadius(10)
                                        .shadow(color: Color.black.opacity(0.3), radius: 6, x: 0, y: 0)
                                        .padding(.top, 15)
                                    Text(item.titles[0])
                                        .lineLimit(2)
                                        .font(.caption)
                                        .foregroundColor(.gray)
                                        .frame(height: 35, alignment: .top)
                                }
                                .frame(width: 120, alignment: .top)
                            }
                        }
                    } else {
                        ZStack {
                            Rectangle()
                                .foregroundColor(.clear)
                                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 12)
                            
                            ActivityIndicator(isAnimating: .constant(true), style: .medium)
                        }
                    }
                }.padding(.horizontal)
            }
        }
    }
}

#if DEBUG
struct GalleryView_Previews: PreviewProvider {
    static var previews: some View {
        GalleryView(title: "Some view", expandTitle: "еще", model: [
            ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ReleasesListItem(id: 6, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: "")
        ])
    }
}
#endif
