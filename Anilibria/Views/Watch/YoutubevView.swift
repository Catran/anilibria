//
//  YoutubevView.swift
//  Anilibria_test
//
//  Created by  Piter Iaskevich on 15.01.2020.
//  Copyright © 2020  Piter Iaskevich. All rights reserved.
//

import SwiftUI

struct YoutubevView: View {
    let model: PreviewModel
    
    var body: some View {
        VStack {
            HStack {
                Text(model.title)
                    .font(.headline)
                Spacer()
                NavigationLink(destination: YoutubeView()) {
                    Text("eще")
                        .font(.headline)
                        .foregroundColor(Color.anilibria.darkRed)
                }
            }.padding(.horizontal)
            model.image
                .renderingMode(.original)
                .resizable()
                .scaledToFit()
                .frame(width: UIScreen.main.bounds.width)
        }
    }
}

struct YoutubevView_Previews: PreviewProvider {
    static var previews: some View {
        YoutubevView(model: PreviewModel(id: 1, title: "Youtube", image: Image("youtube"), description: nil))
    }
}
