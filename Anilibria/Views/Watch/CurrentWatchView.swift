//
//  CurrentWatchView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 03.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct CurrentWatchView: View {
    @State private var title = ""
    @State private var posterUrl = ""
    @EnvironmentObject private var userSettings: UserSettings
    
    func load() {
        NetworkService.shared.fetchRelease(id: self.userSettings.currentWatchId, filters: [.names, .poster]) { (release) in
            guard let title = release.names?[0], let url = release.poster else { return }
            self.title = title
            self.posterUrl = url
        }
    }
    
    
    var body: some View {
        VStack {
            HStack {
                Text(title)
                    .font(.headline)
                Spacer()
            }.padding(.horizontal).padding(.top)
            ZStack {
                URLImage(imageUrl: posterUrl)
                    .scaledToFill()
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 5)
                    .clipped()
                
                HStack{
                    Spacer()
                    NavigationLink(destination: ReleaseDetail(id: self.userSettings.currentWatchId)) {
                        PlayButton().frame(width: 65, height: 65)
                        
                    }
                    Spacer()
                }
            }
        }
        .onAppear(perform: { self.load() })
    }
}

struct CurrentWatchView_Previews: PreviewProvider {
    static var previews: some View {
        CurrentWatchView()
    }
}
