//
//  MediathequeView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 29.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct MediathequeView: View {
    let title: String
    
    var body: some View {
        VStack {
            Text("Раздел находиться в разработке")
                .font(.largeTitle)
                .navigationBarTitle(Text(title), displayMode: .automatic)
            Spacer()
        }.padding()
    }
}

struct MediathequeView_Previews: PreviewProvider {
    static var previews: some View {
        MediathequeView(title: "Медитатека")
    }
}
