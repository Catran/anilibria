//
//  CellSearchView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 29.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct CellSearchView: View {
    let releaseItem: ReleasesListItem
    var body: some View {
        HStack(alignment: .top, spacing: 10) {
            URLImage(imageUrl: releaseItem.poster)
                .scaledToFit()
                .frame(height: 160)
                .cornerRadius(10)
                .shadow(color: Color.black.opacity(0.3), radius: 6, x: 0, y: 0)
            
            VStack(alignment: .leading, spacing: 5) {
                Text(releaseItem.titles[0])
                    .font(.body)
                    .lineLimit(2)
                
                Text(releaseItem.description)
                    .font(.footnote)
                    .lineLimit(7)
            }.frame(maxHeight: 160)
        }
    }
}

struct CellSearchView_Previews: PreviewProvider {
    static var previews: some View {
        CellSearchView(releaseItem: ReleasesListItem(id: 1202, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: "https://img1.ak.crunchyroll.com/i/spire1/5601eec39026ea5dbf88fc35d9d6fb9f1516836642_full.jpg"))
    }
}
