//
//  SearchFilterView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 30.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct SearchFilterView: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var releasesList: ReleasesList
    
    var body: some View {
         NavigationView {
           VStack {
               ScrollView(.vertical) {
                    VStack(alignment: .leading, spacing: 20) {
                        Toggle(isOn: $releasesList.filterState.isComplite.animation(.linear)) {
                            Text("Релиз завершен")
                                .foregroundColor(colorScheme == .dark ? .gray : .black)
                        }
                        
                        SelectedSortView(state: $releasesList.filterState.state)
                        
                        SelectedYearsView(years: $releasesList.filterState.years)
                        
                        SelectedGenersView(geners: $releasesList.filterState.genres)
                        
                        SelectedSeasonsView(seasons: $releasesList.filterState.seasons)
                        
                    }.padding(.horizontal, 2)
                }
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                    self.releasesList.isFilter = true
                    self.releasesList.clearReleasesListItems()
                    self.releasesList.loadMoreReleasesItems()
                }) {
                    Text("Найти аниме")
                       .font(Font.system(size:20))
                       .foregroundColor(.white)
                       .multilineTextAlignment(.center)
                       .padding(.vertical, 5)
                       .padding(.horizontal, 35)
                       .background(
                           Rectangle()
                               .cornerRadius(20)
                               .foregroundColor(Color.anilibria.red)
                       )
                }.padding(.bottom, 20)
                Button(action: {
                  withAnimation(.spring()) {
                    self.releasesList.filterState.isComplite = false
                    self.releasesList.filterState.state = 1
                    self.releasesList.filterState.genres = nil
                    self.releasesList.filterState.years = nil
                    self.releasesList.filterState.seasons = nil
                  }
                }) {
                   Text("Сбросить фильтры").foregroundColor(Color.anilibria.darkRed)
                }
           }.padding().navigationBarTitle(Text("Филтры"), displayMode: .inline)
       }
    }
}

struct SearchFilterView_Previews: PreviewProvider {
    static var previews: some View {
        SearchFilterView()
    }
}

struct SelectedSortView: View {
    @Binding var state: Int
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("Сортировать")
                .font(.footnote)
                .foregroundColor(Color.anilibria.darkRed)
            HStack {
                Text("По популярности")
                    .foregroundColor(colorScheme == .dark ? .gray : .black)
                Spacer()
                if state == 1 {
                    Image(systemName: "checkmark")
                        .foregroundColor(Color.anilibria.darkRed)
                }
            }.padding(.vertical, 5)
                .onTapGesture {
                    self.state = 1
            }
            Divider()
            HStack {
                Text("По новизне")
                    .foregroundColor(colorScheme == .dark ? .gray : .black)
                Spacer()
                if state == 2 {
                    Image(systemName: "checkmark")
                        .foregroundColor(Color.anilibria.darkRed)
                }
            }.padding(.vertical, 5)
                .onTapGesture {
                    self.state = 2
            }
        }
    }
}

struct SelectedYearsView: View {
    @Binding var years: String?
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    @ObservedObject private var viewModel = YearsViewModel()
    
    init(years: Binding<String?>) {
        _years = years
        
        if viewModel.years.isEmpty {
            viewModel.fetch()
        }
        
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("Года")
                .font(.footnote)
                .foregroundColor(Color.anilibria.darkRed)
            
            NavigationLink(destination: MultipleSelectionList(items: $viewModel.years, pattern: $years)) {
                Text(years ?? "Все года...")
                    .foregroundColor(colorScheme == .dark ? .gray : .black)
            }
        }
    }
}

struct SelectedGenersView: View {
    @Binding var geners: String?
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    @ObservedObject private var viewModel = GenresViewModel()
    
    init(geners: Binding<String?>) {
        _geners = geners
        
        if viewModel.genres.isEmpty {
            viewModel.fetch()
        }
        
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("Жанры")
                .font(.footnote)
                .foregroundColor(Color.anilibria.darkRed)
            NavigationLink(destination: MultipleSelectionList(items: $viewModel.genres, pattern: $geners)) {
                Text(geners ?? "Все жанры...")
                    .foregroundColor(colorScheme == .dark ? .gray : .black)
            }
        }
    }
}

struct SelectedSeasonsView: View {
    @Binding var seasons: String?
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    @State private var seasonsList = ["Зима", "Весна", "Лето", "Осень"]
    
    init(seasons: Binding<String?>) {
        _seasons = seasons
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("Сезоны")
                .font(.footnote)
                .foregroundColor(Color.anilibria.darkRed)
            
            NavigationLink(destination: MultipleSelectionList(items: $seasonsList, pattern: $seasons)) {
                Text(seasons ?? "Все сезоны...")
                    .foregroundColor(colorScheme == .dark ? .gray : .black)
            }
        }
    }
}
