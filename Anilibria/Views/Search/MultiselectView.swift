//
//  MultiListView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 30.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct MultipleSelectionList: View {
    @Binding var items: [String]
    @State var selections: [String] = []
    @Binding var pattern: String?
    private var titleAllToggle: String {
        if !self.selections.isEmpty {
            return "Снять выделение"
        }
        return "Выделить все"
    }
    
    init(items: Binding<[String]>, pattern: Binding<String?>) {
        _items = items
        _pattern = pattern
    }
    
    private func initState() {
        if _pattern.wrappedValue != nil {
            var raw = _pattern.wrappedValue!
            raw = raw.replacingOccurrences(of: ", ", with: ",")
            self.selections = raw.split(separator: ",").map { String($0) }
        } else {
            self.selections = []
        }
    }
    
    private func convertToString() {
        self.selections.sort(by: >)
        let selectedItems = self.selections.joined(separator: ", ")
        self.pattern = selectedItems.isEmpty ? nil : selectedItems
    }
    
    private func allToggle() {
        if self.selections.isEmpty {
            self.selections = self.items
        } else {
            self.selections = []
        }
    }

    var body: some View {
        List {
            Button(action: { self.allToggle() }) {
                HStack {
                    Spacer()
                    Text(titleAllToggle)
                        .font(.body)
                        .foregroundColor(Color.anilibria.darkRed)
                        .padding()
                    Spacer()
                }
            }
            ForEach(self.items, id: \.self) { item in
                MultipleSelectionRow(title: item, isSelected: self.selections.contains(item)) {
                    if self.selections.contains(item) {
                        self.selections.removeAll(where: { $0 == item })
                    }
                    else {
                        self.selections.append(item)
                    }
                }
            }
        }
        .onAppear(perform: { self.initState() })
        .onDisappear(perform: { self.convertToString() })
    }
}

struct MultipleSelectionRow: View {
    var title: String
    var isSelected: Bool
    var action: () -> Void
    
    @Environment(\.colorScheme) private var colorScheme: ColorScheme
    
    private var textColor : Color {
        return colorScheme == .dark ? .gray : .black
    }

    var body: some View {
        Button(action: self.action) {
            HStack {
                Text(self.title)
                    .foregroundColor(textColor)
                if self.isSelected {
                    Spacer()
                    Image(systemName: "checkmark")
                        .foregroundColor(Color.anilibria.darkRed)
                }
            }
        }
    }
}

struct MultipleSelectionList_Previews: PreviewProvider {
    static var previews: some View {
        MultipleSelectionList(items: .constant(["One", "Two", "Three"]), pattern: .constant("One, Two"))
    }
}
