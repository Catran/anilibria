//
//  SearchView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 29.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct SearchView: View {
    
    let title: String
    @State private var searchText = ""
    @State private var showFilters = false
    @EnvironmentObject var releasesList: ReleasesList
    
    func load() {
        self.releasesList.isFilter = true
        self.releasesList.loadMoreReleasesItems()
    }
    
    var body: some View {
        VStack {
            SearchBar(text: self.$searchText, placeholder: "Найти анимешку")
            if releasesList.isEmpty {
                Image("ErrorNotFound")
                    .renderingMode(.original)
                    .resizable()
                    .scaledToFit()
                Spacer()
            } else {
                List(releasesList) { item in
                    NavigationLink(destination: ReleaseDetail(id: item.id, image: nil)) {
                        CellSearchView(releaseItem: item)
                            .onAppear(perform: { self.releasesList.loadMoreReleasesItems(item) })
                    }
                }
            }
        }
        .navigationBarTitle(Text(title), displayMode: .inline)
        .navigationBarItems(trailing:
            Button(action: { self.showFilters = true }) {
                Image(systemName: "line.horizontal.3.decrease.circle").font(.title)
            }.sheet(isPresented: $showFilters) {
                SearchFilterView().environmentObject(self.releasesList)
            }
        )
    }
}

