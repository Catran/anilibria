//
//  ScheduleCurrentDayView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 02.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct ScheduleCurrentDayView: View {
    let title: String
    let expandTitle: String
    let model: [[ReleasesListItem]]
    
    private var getCurrentDay: Int {
        var day = Calendar(identifier: .gregorian).component(.weekday, from: Date()) - 2
        if day == -1 {
            day = 6
        }
        return day
    }
    
    var body: some View {
        VStack(spacing: 0) {
            HStack {
                Text(title)
                    .font(.headline)
                Spacer()
                NavigationLink(destination:  ScheduleView(model: model)) {
                    Text(expandTitle)
                        .font(.headline)
                        .foregroundColor(Color.anilibria.darkRed)
                }
            }.padding(.horizontal)
            if !self.model[getCurrentDay].isEmpty {
                GalleryView(title: "", expandTitle: "", model: self.model[getCurrentDay])
            } else {
                HStack {
                    Spacer()
                    Text("На сегодня релизов не запланировано")
                        .font(.title)
                        .padding(.vertical, 30)
                        .multilineTextAlignment(.center)
                    Spacer()
                }
            }
        }
    }
}

struct ScheduleCurrentDayView_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleCurrentDayView(title: "Ожидается сегодня", expandTitle: "", model: [
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ]
        ])
    }
}
