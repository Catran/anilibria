//
//  ScheduleView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 02.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct ScheduleView: View {
    let model: [[ReleasesListItem]]
    private let weekdays = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: true){
            VStack {
                ForEach(0..<model.count) { i in
                    if !self.model[i].isEmpty {
                        GalleryView(title: self.weekdays[i], expandTitle: "", model: self.model[i])
                    }
                }
            }.padding(.top, 20)
        }.navigationBarTitle(Text("Расписание"), displayMode: .inline)
    }
}

struct ScheduleView_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleView(model: [
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ],
            [
                ReleasesListItem(id: 1, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 2, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 3, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 4, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
                ReleasesListItem(id: 5, titles: ["Волчица и пряности", "Spice and wolf"], description: "", poster: ""),
            ]
        ])
    }
}
