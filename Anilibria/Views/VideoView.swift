//
//  VideoView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 27.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct VideoView: View {
    let toPresent = UIHostingController(rootView: AnyView(EmptyView()))
    let url : String
    @ObservedObject private var viewModel = VideoPlayerViewModel()
    
    func load() {
        if self.viewModel.player == nil {
            self.viewModel.fetch(url: self.url)
        }
    }
    
    var body: some View {
        AVPlayerView(player: self.viewModel.player)
            .transition(.move(edge: .bottom))
            .edgesIgnoringSafeArea(.all)
            .onAppear(perform: { self.load() })
            .onDisappear(perform: {
                if let player = self.viewModel.player {
                    player.pause()
                    player.currentItem?.asset.cancelLoading()
                    player.cancelPendingPrerolls()
                    player.replaceCurrentItem(with: nil)
                    self.viewModel.player = nil
                }
            })
    }
}

struct VideoView_Previews: PreviewProvider {
    static var previews: some View {
        VideoView(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")
    }
}
