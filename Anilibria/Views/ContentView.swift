//
//  ContentView.swift
//  Anilibria_test
//
//  Created by  Piter Iaskevich on 15.01.2020.
//  Copyright © 2020  Piter Iaskevich. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    let navbarAttr: [NSAttributedString.Key : Any] = [.foregroundColor: UIColor.white]
    let tabbarItems: [(icon: String, title: String)] = [
        ("tv", "Смотреть"),
        ("rectangle.stack", "Медиатека"),
        ("magnifyingglass", "Поиск"),
        ]
    
    @State var selectedView : Int = 0
    
    init() {
        UINavigationBar.appearance().backgroundColor = UIColor(named: "mainRed")
        UINavigationBar.appearance().largeTitleTextAttributes = navbarAttr
        UINavigationBar.appearance().titleTextAttributes = navbarAttr
        UINavigationBar.appearance().barTintColor = UIColor(named: "mainRed")
        UINavigationBar.appearance().tintColor = .white
        
        UISwitch.appearance().onTintColor = UIColor(named: "darkRed")
    }
    
    var body: some View {
        TabView(selection: $selectedView) {
            NavigationView {
                WatchView(title: tabbarItems[0].title)
            }.tabItem {
                Image(systemName: tabbarItems[0].icon)
                Text(tabbarItems[0].title)
            }.tag(0)
        
            NavigationView {
                MediathequeView(title: tabbarItems[1].title)
            }.tabItem {
                Image(systemName: tabbarItems[1].icon)
                Text(tabbarItems[1].title)
            }.tag(1)
        
            NavigationView {
                SearchView(title: tabbarItems[2].title)
            }.tabItem {
                Image(systemName: tabbarItems[2].icon)
                Text(tabbarItems[2].title)
            }.tag(2)
        }.accentColor(Color.anilibria.darkRed)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("iPhone SE")
                .colorScheme(.dark)
            
            ContentView()
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("iPhone XS Max")
                .colorScheme(.dark)
        }
    }
}
