//
//  NetworkService.swift
//  Anilibria
//
//  Created by Петр Яскевич on 11.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import Foundation

class NetworkService {
 
    static let shared = NetworkService()
    
    // MARK: - Varibles
    private let ssid = "PCpN2a0HaEmjUFJ0QUB9dcCtt8"
    private var request: URLRequest!
    private var session: URLSession!
    
    // MARK: - Initialization
    init() {
        guard let url = URL(string: "https://www.anilibria.tv/public/api/index.php") else { return }
        self.request = URLRequest(url: url)
        self.request.httpMethod = "POST"
        self.request.addValue("PHPSESSID", forHTTPHeaderField: self.ssid)
        
        let config = URLSessionConfiguration.default
//        config.connectionProxyDictionary = [AnyHashable: Any]()
//        config.connectionProxyDictionary?["HTTPSProxy"] = "5.187.0.24"
//        config.connectionProxyDictionary?["HTTPSPort"] = 3128
        
        self.session = URLSession(configuration: config)
    }
    
    // MARK: - Private methods
    private func filterToString(_ filters: [Filter]) ->String {
        let names = filters.map { $0.rawValue }
        return names.joined(separator: ",")
    }
    
    private func fetchRelease(_ data: Data, complition: @escaping (_ release: Release)->()) {
        self.request.httpBody = data
        self.session.dataTask(with: self.request) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONDecoder().decode(BasicOneReleaseResponse.self, from: data)
                DispatchQueue.main.async {
                    complition(json.data)
                }
            } catch let error {
                print(error)
            }
        }.resume()
    }
    
    private func fetchReleases(_ data: Data, complition: @escaping (_ releases: [Release], _ pagination: Pagination)->()) {
        self.request.addValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        self.request.httpBody = data
        self.session.dataTask(with: self.request) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONDecoder().decode(BasicListResponse.self, from: data)
                DispatchQueue.main.async {
                    complition(json.data.items ?? [], json.data.pagination)
                }
            } catch {
                do {
                    let json = try JSONDecoder().decode(BasicReleasesResponse.self, from: data)
                    DispatchQueue.main.async {
                        complition(json.data ?? [], Pagination(page: 1, perPage: json.data?.count ?? 0, allPages: 1, allItems: json.data?.count ?? 0))
                    }
                } catch let error {
                    print(error)
                }
            }
        }.resume()
    }
    
    private func fetchSchedule(_ data: Data, complition: @escaping (_ schedule: [Schedule])->()) {
        self.request.httpBody = data
        self.session.dataTask(with: self.request) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONDecoder().decode(BasicSchedluleResponse.self, from: data)
                DispatchQueue.main.async {
                    complition(json.data)
                }
            } catch let error {
                print(error)
            }
        }.resume()
    }
    
    private func fetchRandomReleaseCode(complition: @escaping (_ code: String)->()) {
         guard let data = String("query=random_release").data(using: .utf8) else { return }
        self.request.httpBody = data
        self.session.dataTask(with: self.request) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONDecoder().decode(BasicRandomReleaseResponse.self, from: data)
                DispatchQueue.main.async {
                    complition(json.data.code)
                }
            } catch let error {
                print(error)
            }
        }.resume()
    }
    
    private func fetchStrings(_ data: Data, complition: @escaping (_ strings: [String])->()) {
        self.request.httpBody = data
        self.session.dataTask(with: self.request) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONDecoder().decode(BasicStringsResponse.self, from: data)
                DispatchQueue.main.async {
                    complition(json.data)
                }
            } catch let error {
                print(error)
            }
        }.resume()
    }
    
    private func fetchYoutubes(_ data: Data, complition: @escaping (_ youtubes: [Youtube], _ pagination: Pagination)->()) {
        self.request.httpBody = data
        self.session.dataTask(with: self.request) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONDecoder().decode(BasicYoutubeResponse.self, from: data)
                DispatchQueue.main.async {
                    complition(json.data.items ?? [], json.data.pagination)
                }
            } catch let error {
                print(error)
            }
        }.resume()
    }
    
    // MARK: - Public methods
    func fetchRelease(id: Int, filters: [Filter] = [], complition: @escaping (_ resleses: Release)->()) {
        var params = "query=release&id=\(id)"
        if !filters.isEmpty {
            params += "&filter=\(filterToString(filters))"
        }
        guard let data = params.data(using: .utf8) else { return }
        self.fetchRelease(data) { (release) in
            complition(release)
        }
    }
    
    func fetchRelease(code: String, filters: [Filter] = [], complition: @escaping (_ resleses: Release)->()) {
        var params = "query=release&code=\(code)"
        if !filters.isEmpty {
            params += "&filter=\(filterToString(filters))"
        }
        guard let data = params.data(using: .utf8) else { return }
        self.fetchRelease(data) { (release) in
            complition(release)
        }
    }
    
    func fetchReleases(sort: Sort,
                       finish: Finish,
                       perPage: Int = 10,
                       page: Int = 1,
                       search: String = "",
                       filters: [Filter] = [],
                       complition: @escaping (_ releases: [Release], _ pagination: Pagination)->()) {
        var params = "query=catalog&perPage=\(perPage)&page=\(page)&sort=\(sort.rawValue)&xpage=catalog&finish=\(finish.rawValue)"
        if search != "" {
            params += "&search=\(search)"
        }
        if !filters.isEmpty {
            params += "&filter=\(filterToString(filters))"
        }
        guard let data = params.data(using: .utf8) else { return }
        fetchReleases(data) { (releases, pagination) in
            complition(releases, pagination)
        }
    }
    
    func fetchSchedule(filters: [Filter] = [],
                       complition: @escaping (_ schedule: [Schedule])->()) {
        var params = "query=schedule"
        if !filters.isEmpty {
            params += "&filter=\(filterToString(filters))"
        }
        guard let data = params.data(using: .utf8) else { return }
        fetchSchedule(data) { (schedule) in
            complition(schedule)
        }
    }
    
    func fetchRandomRelease (complition: @escaping (_ code: String)->()) {
        fetchRandomReleaseCode { (code) in
            complition(code)
        }
    }
    
    func fetchGenres(complition: @escaping (_ geners: [String])->()) {
        let params = "query=genres"
        guard let data = params.data(using: .utf8) else { return }
        fetchStrings(data) { (strings) in
            complition(strings)
        }
    }
    
    func fetchYears(complition: @escaping (_ years: [String])->()) {
        let params = "query=years"
        guard let data = params.data(using: .utf8) else { return }
        fetchStrings(data) { (strings) in
            complition(strings)
        }
    }
    
    func fetchSearch(search: String,
                     perPage: Int,
                     finish: Finish,
                     page: Int = 1,
                     filters: [Filter] = [],
                     complition: @escaping (_ releases: [Release], _ pagination: Pagination)->()) {
        var params = "query=search&search=\(search)&perPage=\(perPage)&page=\(page)&finish\(finish.rawValue)"
        if !filters.isEmpty {
            params += "&filter=\(filterToString(filters))"
        }
        guard let data = params.data(using: .utf8) else { return }
        fetchReleases(data) { (releases, pagination) in
            complition(releases, pagination)
        }
    }
    
    func fetchYoutubes(perPage: Int,
                      page: Int = 1,
                      complition: @escaping (_ youtubes: [Youtube], _ pagination: Pagination)->()) {
        let params = "query=youtube&perPage=\(perPage)&page=\(page)"
        guard let data = params.data(using: .utf8) else { return }
        fetchYoutubes(data) { (youtubes, pagination) in
            complition(youtubes, pagination)
        }
    }
    
    func fetchImage(url: String, complition: @escaping (_ image: Image )->()) {
        guard let fullUrl = URL(string: "https://www.anilibria.tv" + url) else { return }
        
        if let cachedResponse = URLCache.shared.cachedResponse(for: URLRequest(url: fullUrl)) {
            complition(Image(uiImage: UIImage(data: cachedResponse.data)!))
            return
        }
        
        self.session.dataTask(with: fullUrl) { data, response, _ in
            guard
                let data = data,
                let image = UIImage(data: data),
                let response = response
            else { return }
            
            DispatchQueue.main.async {
                let cachedResponse = CachedURLResponse(response: response, data: data)
                URLCache.shared.storeCachedResponse(cachedResponse, for: URLRequest(url: response.url!))
                complition(Image(uiImage: image))
            }
        }.resume()
    }
}

extension NetworkService {
    enum Sort: String {
        case newness = "1"
        case favorites = "2"
    }
}

extension NetworkService {
    enum Filter: String {
        case code = "code"
        case names = "names"
        case series = "series"
        case poster = "poster"
        case favorite = "favorite"
        case last = "last"
        case moon = "moon"
        case status = "status"
        case type = "type"
        case genres = "genres"
        case voices = "voices"
        case year = "year"
        case day = "day"
        case description = "description"
        case playlist = "playlist"
    }
}

extension NetworkService {
    enum Finish: String {
        case all = "1"
        case completed = "2"
    }
}

