//
//  ReleasesList.swift
//  Anilibria
//
//  Created by Петр Яскевич on 01.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

class YoutubeList: ObservableObject, RandomAccessCollection {
    typealias Element = YoutubeListItem
    typealias Index = Int
    
    var startIndex: Int = 0
    var endIndex: Int { youtubeListItems.endIndex }
    
    @Published var youtubeListItems = [YoutubeListItem]()
    
    var nextPageToLoad = 1
    var doneLoading = false
    var currentlyLoading = false
    var isFilter = true
    
    init() {
        loadMoreYoutubeItems()
    }
    
    subscript(position: Int) -> YoutubeListItem {
        return youtubeListItems[position]
    }
    
    func loadMoreYoutubeItems(_ youtubeListItem: YoutubeListItem? = nil) {
        if !shouldLoadMoreItems(youtubeListItem) {
            return
        }
        
        fetchYoutube { (youtubes) in
            for youtube in youtubes {
                self.youtubeListItems.append(youtube)
            }
            self.nextPageToLoad += 1
            self.doneLoading = (youtubes.count == 0)
            self.currentlyLoading = false
        }
        
        currentlyLoading = true
    }
    
    private func shouldLoadMoreItems(_ youtubeListItem: YoutubeListItem? = nil) -> Bool {
        if doneLoading || currentlyLoading {
            return false
        }
        
        guard let youtubeListItem = youtubeListItem else {
            return true
        }
        
        for i in (youtubeListItems.count-4)...(youtubeListItems.count-1) {
            if i >= 0 && youtubeListItems[i].uuid == youtubeListItem.uuid {
                return true
            }
        }
        
        return false
    }
    
    
    private func fetchYoutube(complition: @escaping (_ releases: [YoutubeListItem])->()) {
        
        NetworkService.shared.fetchYoutubes(perPage: 12, page: self.nextPageToLoad) { (youtubes, _) in
            var fetchListItems = [YoutubeListItem]()
            
            for youtube in youtubes {
                fetchListItems.append(YoutubeListItem(titles: youtube.title, vid: youtube.vid, poster: youtube.image))
            }
            complition(fetchListItems)
        }
    }
    
    
}
