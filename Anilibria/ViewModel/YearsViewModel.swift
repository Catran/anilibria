//
//  YearssViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 30.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

public class YearsViewModel: ObservableObject {
    @Published var years = [String]()
    
    func fetch() {
        self.years = []
        
        NetworkService().fetchYears() { years in
            self.years = years
        }
    }
}
