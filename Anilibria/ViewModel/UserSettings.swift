//
//  UserSettings.swift
//  Anilibria
//
//  Created by Петр Яскевич on 03.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

class UserSettings: ObservableObject {
    @Published var currentWatchId: Int = UserDefaults.standard.integer(forKey: "CurrentWatchId") {
        didSet { UserDefaults.standard.set(self.currentWatchId, forKey: "CurrentWatchId") }
    }
}
