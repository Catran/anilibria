//
//  GenersViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 30.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

public class GenresViewModel: ObservableObject {
    @Published var genres = [String]()
    
    func fetch() {
        self.genres = []
        
        NetworkService().fetchGenres() { genres in
            self.genres = genres
        }
    }
}
