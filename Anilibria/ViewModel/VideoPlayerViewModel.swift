//
//  VideoPlayerViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 27.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import AVKit

public class VideoPlayerViewModel: ObservableObject {
    @Published var player : AVPlayer? = nil
    
    func fetch(url: String) {
        self.player = nil
        
        DispatchQueue.global(qos: .background).async {
            guard let streamUrl = URL(string: url) else { return }
            let assets = AVURLAsset(url: streamUrl)
            let playerItem = AVPlayerItem(asset: assets)
            let player = AVPlayer(playerItem: playerItem)
            do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            } catch let error {
                print(error.localizedDescription)
            }
            DispatchQueue.main.async {
                self.player = player
            }
        }
    }
}
