//
//  ReleaseViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 25.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import Foundation

public class ReleaseViewModel: ObservableObject {
    @Published var release : Release? = nil
    @Published var poster : Image? = nil
    @Published var firstSeriesUrl: String? = nil
    
    func fetchId(id: Int) {
        self.release = nil
        self.poster = nil
        self.firstSeriesUrl = nil
        
        NetworkService.shared.fetchRelease(id: id) { (release) in
            guard let url = release.poster else { return }
            NetworkService().fetchImage(url: url) { (image) in
                self.release = release
                self.poster = image
                if !release.playlist!.isEmpty {
                    self.firstSeriesUrl = release
                        .playlist?.reversed()[0]
                        .sd
                        .replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                } else {
                    self.firstSeriesUrl = nil
                }
            }
        }
    }
    
    func fetchCode(code: String) {
        self.release = nil
        self.poster = nil
        self.firstSeriesUrl = nil
        
        NetworkService.shared.fetchRelease(code: code) { (release) in
            guard let url = release.poster else { return }
            NetworkService().fetchImage(url: url) { (image) in
                self.release = release
                self.poster = image
                self.firstSeriesUrl = release
                    .playlist?.reversed()[0]
                    .sd
                    .replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
            }
        }
    }
    
    func fetchWithoutImage(id: Int, image: Image) {
        self.release = nil
        self.poster = nil
        self.firstSeriesUrl = nil
        
        NetworkService().fetchRelease(id: id) { (release) in
            self.release = release
            self.poster = image
            self.firstSeriesUrl = release
            .playlist?.reversed()[0]
            .sd
            .replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
        }
    }
}
