//
//  WatchViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 12.01.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import Foundation

public class WatchViewModel: ObservableObject {
    
    @Published var schedule = [[ReleasesListItem]]()
    @Published var bests = [ReleasesListItem]()
    @Published var youtube = PreviewModel(id: 0, title: "YouTube контент", image: Image("youtube"), description: nil)
    @Published var randomRelease: String? = nil
    
    func fetchSchedule() {
        self.schedule = [[ReleasesListItem]]()
        
        NetworkService.shared.fetchSchedule(filters: [.names, .poster]) { (schedule) in
            for day in schedule {
                if let releases = day.items {
                    var current = [ReleasesListItem]()
                    for release in releases {
                        guard
                            let names = release.names,
                            let poster = release.poster
                        else { return }
                        current.append(ReleasesListItem(id: release.id, titles: names, description: "", poster: poster))
                    }
                    self.schedule.append(current)
                }
            }
        }
    }
    
    func fetchBest() {
        self.bests = [ReleasesListItem]()
        
        NetworkService.shared.fetchReleases(sort: .favorites, finish: .all, filters: [.names, .poster, .description]) { (releases, _) in
            for release in releases {
                guard
                    let names = release.names,
                    let description = release.description,
                    let poster = release.poster
                else { return }
                
                self.bests.append(ReleasesListItem(id: release.id, titles: names, description: description, poster: poster))
            }
        }
    }
    
    func fetchYotube(_ perPage: Int) {
        self.youtube = PreviewModel(id: 0, title: "YouTube контент", image: Image("youtube"), description: nil)
        
        NetworkService.shared.fetchYoutubes(perPage: perPage) { (youtube, _) in
            NetworkService().fetchImage(url: youtube[0].image) { (image) in
                self.youtube = PreviewModel(id: youtube[0].id, title: youtube[0].title, image: image, description: nil)
            }
        }
    }
    
    func fetchRandomRelease() {
        self.randomRelease = nil
        
        NetworkService.shared.fetchRandomRelease { (code) in
            self.randomRelease = code
        }
    }
}
