//
//  EpisodeViewModel.swift
//  Anilibria
//
//  Created by Петр Яскевич on 26.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import Foundation
import AVKit

public class EpisodeViewModel: ObservableObject {
    @Published var thumb : Image? = nil

    func fetchThumb(url: String) {
        self.thumb = nil
    }
}
