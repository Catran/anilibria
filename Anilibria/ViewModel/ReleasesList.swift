//
//  ReleasesList.swift
//  Anilibria
//
//  Created by Петр Яскевич on 01.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import Foundation

class ReleasesList: ObservableObject, RandomAccessCollection {
    typealias Element = ReleasesListItem
    typealias Index = Int
    
    var startIndex: Int = 0
    var endIndex: Int { releasesListItems.endIndex }
    
    @Published var releasesListItems = [ReleasesListItem]()
    @Published var filterState = FilterState(isComplite: false, state: 1, years: nil, genres: nil, seasons: nil)
    
    var nextPageToLoad = 1
    var doneLoading = false
    var currentlyLoading = false
    var isFilter = true
    
    init() {
        loadMoreReleasesItems()
    }
    
    subscript(position: Int) -> ReleasesListItem {
        return releasesListItems[position]
    }
    
    func loadMoreReleasesItems(_ releasesListItem: ReleasesListItem? = nil, search: String? = nil) {
        if !shouldLoadMoreItems(releasesListItem) {
            return
        }
        
        currentlyLoading = true
        
        if isFilter {
            fetchWithFilter() { (releases) in
                for release in releases {
                    self.releasesListItems.append(release)
                }
                self.nextPageToLoad += 1
                self.doneLoading = (releases.count == 0)
                self.currentlyLoading = false
            }
        } else {
            guard let search = search else { return }
            
            fetchWithString(text: search) { (releases) in
                for release in releases {
                    self.releasesListItems.append(release)
                }
                self.nextPageToLoad += 1
                self.doneLoading = (releases.count == 0)
                self.currentlyLoading = false
            }
        }
    }
    
    func clearReleasesListItems() {
        self.releasesListItems = [ReleasesListItem]()
        self.nextPageToLoad = 1
        self.doneLoading = false
        self.currentlyLoading = false
    }
    
    private func shouldLoadMoreItems(_ releasesListItem: ReleasesListItem? = nil) -> Bool {
        if doneLoading || currentlyLoading {
            return false
        }
        
        guard let releasesListItem = releasesListItem else {
            return true
        }
        
        for i in (releasesListItems.count-4)...(releasesListItems.count-1) {
            if i >= 0 && releasesListItems[i].uuid == releasesListItem.uuid {
                return true
            }
        }
        
        return false
    }
    
    
    private func fetchWithFilter(complition: @escaping (_ releases: [ReleasesListItem])->()) {
        let sort = filterState.state == 1 ? NetworkService.Sort.favorites :  NetworkService.Sort.newness
        let finish = filterState.isComplite ? NetworkService.Finish.completed : NetworkService.Finish.all
        
        let search = getSearchString()
        
        let filterColumns: [NetworkService.Filter] = [.names, .description, .poster]
        
        NetworkService.shared
            .fetchReleases(sort: sort, finish: finish, perPage: 12, page: self.nextPageToLoad, search: search, filters: filterColumns) { (releases, _) in
                
                var fetchListItems = [ReleasesListItem]()
                
                for release in releases {
                    guard
                        let names = release.names,
                        let description = release.description,
                        let poster = release.poster
                    else { return }
                    
                    fetchListItems.append(ReleasesListItem(id: release.id, titles: names, description: description, poster: poster))
                }
                
                complition(fetchListItems)
        }
    }
    
    private func fetchWithString(text: String, complition: @escaping (_ releases: [ReleasesListItem])->()) {
        
        
        let filterColumns: [NetworkService.Filter] = [.names, .description, .poster]
        
        NetworkService.shared.fetchSearch(search: text, perPage: 12, finish: .all, page: self.nextPageToLoad, filters: filterColumns) { (releases, _) in
            
            var fetchListItems = [ReleasesListItem]()
            
            for release in releases {
                guard
                    let names = release.names,
                    let description = release.description,
                    let poster = release.poster
                else { return }
                
                fetchListItems.append(ReleasesListItem(id: release.id, titles: names, description: description, poster: poster))
            }
            
            complition(fetchListItems)
        }
    }
    
    private func getSearchString()-> String {
        var lgenres = ""
        if filterState.genres != nil {
            lgenres = filterState.genres!.replacingOccurrences(of: ", ", with: ",")
        }
        
        var lyears = ""
        if filterState.years != nil {
            lyears = filterState.years!.replacingOccurrences(of: ", ", with: ",")
        }
        
        var lseasons = ""
        if filterState.seasons != nil {
            lseasons = filterState.seasons!.replacingOccurrences(of: ", ", with: ",")
        }
        
        return "{\"genre\":\"" + lgenres + "\", \"year\":\"" + lyears + "\", \"season\":\"" + lseasons + "\"}"
    }
}
