//
//  WebView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 03.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import WebKit
  
struct WebView : UIViewRepresentable {
      
    let url: String
      
    func makeUIView(context: Context) -> WKWebView  {
        guard let url = URL(string: self.url) else { return WKWebView() }
        let request = URLRequest(url: url)
        let wkWebView = WKWebView()
        wkWebView.load(request)
        return wkWebView
    }
      
    func updateUIView(_ uiView: WKWebView, context: Context) {
        
    }
      
}
  
#if DEBUG
struct WebView_Previews : PreviewProvider {
    static var previews: some View {
        WebView(url: "https://www.apple.com")
    }
}
#endif
