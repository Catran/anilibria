//
//  SearchBar.swift
//  Anilibria
//
//  Created by Петр Яскевич on 29.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import UIKit
import SwiftUI

struct SearchBar: UIViewRepresentable {

    @Binding var text: String
    var placeholder: String
    @EnvironmentObject var releasesList: ReleasesList

    class Coordinator: NSObject, UISearchBarDelegate {

        @Binding var text: String
        @EnvironmentObject var releasesList: ReleasesList

        init(text: Binding<String>, releasesList: EnvironmentObject<ReleasesList>) {
            _text = text
            _releasesList = releasesList
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Отмена"
        }

        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            text = searchText
            searchBar.setShowsCancelButton(true, animated: true)
        }
        
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            searchBar.setShowsCancelButton(true, animated: true)
        }
        
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            text = ""
            searchBar.setShowsCancelButton(false, animated: true)
            searchBar.endEditing(true)
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.endEditing(true)
            releasesList.isFilter = false
            releasesList.clearReleasesListItems()
            releasesList.loadMoreReleasesItems(search: text)
        }
    }

    func makeCoordinator() -> SearchBar.Coordinator {
        return Coordinator(text: $text, releasesList: _releasesList)
    }

    func makeUIView(context: UIViewRepresentableContext<SearchBar>) -> UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.delegate = context.coordinator
        searchBar.searchBarStyle = .minimal
        searchBar.autocapitalizationType = .none
        searchBar.backgroundColor = UIColor(named: "mainRed")
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = placeholder
        
        searchBar.setText(color: .white)
        searchBar.setPlaceholderText(color: .yellow)
        searchBar.setSearchImage(color: .white)
        searchBar.setClearButton(color: .white)
        searchBar.setTextField(color: UIColor.red.withAlphaComponent(0.03))
        return searchBar
    }

    func updateUIView(_ uiView: UISearchBar, context: UIViewRepresentableContext<SearchBar>) {
        uiView.text = text
    }
}
