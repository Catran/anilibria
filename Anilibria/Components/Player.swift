//
//  Player.swift
//  Anilibria
//
//  Created by Петр Яскевич on 26.03.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import AVKit

struct AVPlayerView: UIViewControllerRepresentable {

    var player: AVPlayer?

    func updateUIViewController(_ playerController: AVPlayerViewController, context: Context) {
        playerController.player = player
        playerController.player?.play()
    }

    func makeUIViewController(context: Context) -> AVPlayerViewController {
        return AVPlayerViewController()
    }
}
