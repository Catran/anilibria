//
//  PlayButtonView.swift
//  Anilibria
//
//  Created by Петр Яскевич on 03.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI

struct PlayButton: View {
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Circle().fill(Color.clear).background(Blur(style: .systemMaterialDark).clipShape(Circle()))
                
                Path { path in
                    let width = geometry.size.width
                    let height = width
                    
                    path.move(to: CGPoint(x: width / 4, y: height / 4 + height / 20 ))
                    path.addQuadCurve(to: CGPoint(x: width / 4 + width / 20, y: height / 4), control: CGPoint(x: width / 4, y: height / 4))
                    path.addLine(to: CGPoint(x: width / 4 * 3 - width / 20, y: height / 2 - height / 25))
                    path.addQuadCurve(to: CGPoint(x: width / 4 * 3 - width / 20, y: height / 2 + height / 25), control: CGPoint(x: width / 4 * 3, y: height / 2))
                    path.addLine(to: CGPoint(x: width / 4 + width / 20, y: height / 4 * 3))
                    path.addQuadCurve(to: CGPoint(x: width / 4, y: height / 4 * 3 - height / 20), control: CGPoint(x: width / 4, y: height / 4 * 3))
                    path.addLine(to: CGPoint(x: width / 4, y: height / 4 + height / 20 ))
                }
                .offsetBy(dx: geometry.size.width / 15.38, dy: 0)
                .fill(Color.anilibria.darkRed)
            }
        }
    }
}

struct PlayButton_Previews: PreviewProvider {
    static var previews: some View {
        PlayButton().frame(width: 200, height: 200)
    }
}
