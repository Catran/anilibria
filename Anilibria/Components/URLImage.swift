//
//  URLImage.swift
//  Anilibria
//
//  Created by Петр Яскевич on 01.04.2020.
//  Copyright © 2020 Petr Iaskevich. All rights reserved.
//

import SwiftUI
import Foundation

struct URLImage: View {
    
    @ObservedObject private var imageLoader: ImageLoader
    
    init(imageUrl: String) {
        imageLoader = ImageLoader(imageUrl: imageUrl)
    }
    var body: some View {
        imageLoader.data
            .renderingMode(.original)
            .resizable()
    }
}

struct URLImage_Previews: PreviewProvider {
    static var previews: some View {
        URLImage(imageUrl: "https://img1.ak.crunchyroll.com/i/spire1/5601eec39026ea5dbf88fc35d9d6fb9f1516836642_full.jpg")
    }
}

class ImageLoader: ObservableObject {
    @Published var data = Image("preloadRelease")
    
    init(imageUrl: String) {
        NetworkService.shared.fetchImage(url: imageUrl) { (image) in
            self.data = image
        }
    }
}
